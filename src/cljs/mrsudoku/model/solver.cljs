(ns mrsudoku.model.solver
  (:require [clojure.set :as set]
            [mrsudoku.model.grid :as g]
            [mrsudoku.model.conflict :as cf]))


(defn gen-lines-par-rows
  [grid y]
  (let [result
        (map #(let [stat (get (g/cell grid % y) :status) x %]
          (if (= stat :empty)
            (let [content (reduce
                            (fn [res n]
                              (let [xx (+ 1 (quot (- x 1) 3))
                                    yy (+ 1 (quot (- y 1) 3))
                                    b-id (+ xx (* (- yy 1) 3))
                                    deja (set/union (cf/values (g/block grid b-id)) (set/union (cf/values (g/row grid y)) (cf/values (g/col grid x))))]
                                    (if (not (contains? deja n))
                                      (conj res n)
                                      res)
                              ))
                            [] (range 1 10))]
              (if (= 0 (count content)) [-1] content))
            []
            ))
        (range 1 10))]
    result
  )
)


(defn gen-graph-lines [domain]
  "return des chemain pour cerer graph
    case1 est 11 case2 est 12 case3 13 etc
    chiffre 1 est 1 , 2 est 2 etc"
  (loop [i 11,dom domain,res []]
    (if (seq dom)
      (recur (inc i) (rest dom) (apply conj res (map #(vector i %) (first dom))))
      res
    )))

(defn gen-graph [lines]
      "Returns a hashmap contating the graph"
      (loop [lis lines,res {}]
        (if (seq lis)
          (let [fir (first (first lis))
                sec (second (first lis))]
            (recur (rest lis) (assoc res
                                        fir {:neigh (conj (:neigh (res fir {:neigh #{}})) sec)}
                                        sec {:neigh (conj (:neigh (res sec {:neigh #{}})) fir)})))
          res
)))


(defn map-merge [map1 map2]
  (reduce (fn [mp [ky vl]] (assoc mp ky (set/union vl (get mp ky {}))))  map1 map2)
  )


(defn max-matching
  ([graph]
    (if (= 0 (count graph))
      {}
      (max-matching graph (filter #(> % 10) (map #(first %) (into [] graph))) #{})
      )
    )
  ([graph nexts deja-match]
      (if (= (count nexts) 1)
        (let [rest-match (set/difference ((graph (first nexts)) :neigh) deja-match)]
            (if (> (count rest-match) 0)
              {(first nexts) rest-match}
              {}
              )
          )

          (loop [match (set/difference ((graph (first nexts)) :neigh) deja-match)
                 ;sub-res (max-matching (rest nexts) (first match))
                 this-res {}
                 res {}]

                (if (seq match)
                  (let [sub-res (max-matching graph (rest nexts) (conj deja-match (first match)))]
                    ;(println sub-res match deja-match (map-merge this-res {(first nexts) (first match)}))
                    (if (> (count sub-res) 0)
                      (recur (rest match) (map-merge this-res {(first nexts) #{(first match)}}) (map-merge res sub-res))
                      (recur (rest match) this-res res)
                      )
                     )
                  (map-merge res this-res)
                  )

            )
      )
    )
  )

(defn gen-dom-row [grid nb-row]
  (let [dom (gen-lines-par-rows grid nb-row)
          lines (gen-graph-lines dom)
          graph (gen-graph lines)]
          ;(println dom)
          [nb-row (max-matching graph)]
          )
  )

(defn affiche-dom [doms]
  (loop [tmp doms]
    (if (seq tmp)
      (do (println (first tmp))
        (recur (rest tmp))
        )
      nil
      )
    )
  )

(defn chercher-min-contraint [doms]
  (loop [tmp doms,x 1,y 1,vals #{1 2 3 4 5 6 7 8 9 10}]
    (if (seq tmp)
    (let [[yy matchs] (first tmp)]
      (if (= 0 (count matchs))
        (recur (rest tmp) x y vals)
        (let [[xx val] (first (sort-by #(count (val %)) matchs))]
          (if (< (count val) (count vals))
              (recur (rest tmp) xx yy val)
              (recur (rest tmp) x y vals)))))
      [(- x 10) y vals]))
  )


(defn gen-doms [grid]
  (into {} (map #(gen-dom-row grid %) (range 1 10)))
  )


(defn update-map
  ([m f]
  (reduce-kv (fn [m k v] (assoc m k (f v))) {} m))
  ([m f keys]
    (reduce-kv (fn [m k v] (if (contains? keys k) (assoc m k (f v)) (assoc m k v))
      ) {} m))
)

(defn row-enlever [doms y vals]
  (update doms y #(update-map % (fn [m] (disj m vals)))))

(defn col-enlever [doms x vals]
  (update-map doms #(update-map % (fn [m] (disj m vals)) #{x})))

(defn block-enlever [doms cx cy vals]
  (let [block-x (+ 10 (+ (* 3 (quot (- cx 1) 3)) 1))
        block-y (+ (* 3 (quot (- cy 1) 3)) 1)
        rx (into #{} (range block-x (+ 3 block-x)))
        ry (into #{} (range block-y (+ 3 block-y)))]
          ;(println rx ry)
          (update-map doms #(update-map % (fn [m] (disj m vals)) rx) ry)
        )
  )

(defn doms-remplir
    ([doms x y vals]
      (let [a (update doms y #(dissoc % (+ 10 x)))
            b (row-enlever a y vals)
            c (col-enlever b (+ x 10) vals)
            d  (block-enlever c x y vals)]
          d
        ))
  )



(defn solve
  "Solve the sudoku `grid` by returing a full solved grid,
 or `nil` if the solver fails."
  ([grid]
    ;(solve grid (gen-doms grid) 1)
    (let [doms (gen-doms grid)]
      (solve grid doms))
  )
  ([grid doms]
  ;; A compléter
    (let [[x y vals] (chercher-min-contraint doms)
          ;new-doms (doms-enlever doms x y)
          solveable (> (first vals) 0)
          solver  (= (count vals) 10)]
      ;(affiche-dom doms)
      ;(println "-----------------" i)
      (if solver
        grid
        (if solveable
          (loop [tmp-vals vals]
            (if (seq tmp-vals)
              (let [tmp-grid (g/change-cell grid x y (g/mk-cell :set (first tmp-vals)))
                    solve-grid (solve tmp-grid (doms-remplir doms x y (first tmp-vals)))]
                  (if solve-grid
                    solve-grid
                    (recur (rest tmp-vals))))
              nil))
          nil))))
)
