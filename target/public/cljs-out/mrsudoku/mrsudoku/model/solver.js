// Compiled by ClojureScript 1.10.339 {}
goog.provide('mrsudoku.model.solver');
goog.require('cljs.core');
goog.require('clojure.set');
goog.require('mrsudoku.model.grid');
goog.require('mrsudoku.model.conflict');
mrsudoku.model.solver.gen_lines_par_rows = (function mrsudoku$model$solver$gen_lines_par_rows(grid,y){
var result = cljs.core.map.call(null,(function (p1__11157_SHARP_){
var stat = cljs.core.get.call(null,mrsudoku.model.grid.cell.call(null,grid,p1__11157_SHARP_,y),new cljs.core.Keyword(null,"status","status",-1997798413));
var x = p1__11157_SHARP_;
if(cljs.core._EQ_.call(null,stat,new cljs.core.Keyword(null,"empty","empty",767870958))){
var content = cljs.core.reduce.call(null,((function (stat,x){
return (function (res,n){
var xx = ((1) + cljs.core.quot.call(null,(x - (1)),(3)));
var yy = ((1) + cljs.core.quot.call(null,(y - (1)),(3)));
var b_id = (xx + ((yy - (1)) * (3)));
var deja = clojure.set.union.call(null,mrsudoku.model.conflict.values.call(null,mrsudoku.model.grid.block.call(null,grid,b_id)),clojure.set.union.call(null,mrsudoku.model.conflict.values.call(null,mrsudoku.model.grid.row.call(null,grid,y)),mrsudoku.model.conflict.values.call(null,mrsudoku.model.grid.col.call(null,grid,x))));
if(!(cljs.core.contains_QMARK_.call(null,deja,n))){
return cljs.core.conj.call(null,res,n);
} else {
return res;
}
});})(stat,x))
,cljs.core.PersistentVector.EMPTY,cljs.core.range.call(null,(1),(10)));
if(cljs.core._EQ_.call(null,(0),cljs.core.count.call(null,content))){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(-1)], null);
} else {
return content;
}
} else {
return cljs.core.PersistentVector.EMPTY;
}
}),cljs.core.range.call(null,(1),(10)));
return result;
});
mrsudoku.model.solver.gen_graph_lines = (function mrsudoku$model$solver$gen_graph_lines(domain){

var i = (11);
var dom = domain;
var res = cljs.core.PersistentVector.EMPTY;
while(true){
if(cljs.core.seq.call(null,dom)){
var G__11159 = (i + (1));
var G__11160 = cljs.core.rest.call(null,dom);
var G__11161 = cljs.core.apply.call(null,cljs.core.conj,res,cljs.core.map.call(null,((function (i,dom,res){
return (function (p1__11158_SHARP_){
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[i,p1__11158_SHARP_],null));
});})(i,dom,res))
,cljs.core.first.call(null,dom)));
i = G__11159;
dom = G__11160;
res = G__11161;
continue;
} else {
return res;
}
break;
}
});
mrsudoku.model.solver.gen_graph = (function mrsudoku$model$solver$gen_graph(lines){

var lis = lines;
var res = cljs.core.PersistentArrayMap.EMPTY;
while(true){
if(cljs.core.seq.call(null,lis)){
var fir = cljs.core.first.call(null,cljs.core.first.call(null,lis));
var sec = cljs.core.second.call(null,cljs.core.first.call(null,lis));
var G__11162 = cljs.core.rest.call(null,lis);
var G__11163 = cljs.core.assoc.call(null,res,fir,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"neigh","neigh",-1718528884),cljs.core.conj.call(null,new cljs.core.Keyword(null,"neigh","neigh",-1718528884).cljs$core$IFn$_invoke$arity$1(res.call(null,fir,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"neigh","neigh",-1718528884),cljs.core.PersistentHashSet.EMPTY], null))),sec)], null),sec,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"neigh","neigh",-1718528884),cljs.core.conj.call(null,new cljs.core.Keyword(null,"neigh","neigh",-1718528884).cljs$core$IFn$_invoke$arity$1(res.call(null,sec,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"neigh","neigh",-1718528884),cljs.core.PersistentHashSet.EMPTY], null))),fir)], null));
lis = G__11162;
res = G__11163;
continue;
} else {
return res;
}
break;
}
});
mrsudoku.model.solver.map_merge = (function mrsudoku$model$solver$map_merge(map1,map2){
return cljs.core.reduce.call(null,(function (mp,p__11164){
var vec__11165 = p__11164;
var ky = cljs.core.nth.call(null,vec__11165,(0),null);
var vl = cljs.core.nth.call(null,vec__11165,(1),null);
return cljs.core.assoc.call(null,mp,ky,clojure.set.union.call(null,vl,cljs.core.get.call(null,mp,ky,cljs.core.PersistentArrayMap.EMPTY)));
}),map1,map2);
});
mrsudoku.model.solver.max_matching = (function mrsudoku$model$solver$max_matching(var_args){
var G__11171 = arguments.length;
switch (G__11171) {
case 1:
return mrsudoku.model.solver.max_matching.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 3:
return mrsudoku.model.solver.max_matching.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

mrsudoku.model.solver.max_matching.cljs$core$IFn$_invoke$arity$1 = (function (graph){
if(cljs.core._EQ_.call(null,(0),cljs.core.count.call(null,graph))){
return cljs.core.PersistentArrayMap.EMPTY;
} else {
return mrsudoku.model.solver.max_matching.call(null,graph,cljs.core.filter.call(null,(function (p1__11168_SHARP_){
return (p1__11168_SHARP_ > (10));
}),cljs.core.map.call(null,(function (p1__11169_SHARP_){
return cljs.core.first.call(null,p1__11169_SHARP_);
}),cljs.core.into.call(null,cljs.core.PersistentVector.EMPTY,graph))),cljs.core.PersistentHashSet.EMPTY);
}
});

mrsudoku.model.solver.max_matching.cljs$core$IFn$_invoke$arity$3 = (function (graph,nexts,deja_match){
if(cljs.core._EQ_.call(null,cljs.core.count.call(null,nexts),(1))){
var rest_match = clojure.set.difference.call(null,graph.call(null,cljs.core.first.call(null,nexts)).call(null,new cljs.core.Keyword(null,"neigh","neigh",-1718528884)),deja_match);
if((cljs.core.count.call(null,rest_match) > (0))){
return cljs.core.PersistentArrayMap.createAsIfByAssoc([cljs.core.first.call(null,nexts),rest_match]);
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
} else {
var match = clojure.set.difference.call(null,graph.call(null,cljs.core.first.call(null,nexts)).call(null,new cljs.core.Keyword(null,"neigh","neigh",-1718528884)),deja_match);
var this_res = cljs.core.PersistentArrayMap.EMPTY;
var res = cljs.core.PersistentArrayMap.EMPTY;
while(true){
if(cljs.core.seq.call(null,match)){
var sub_res = mrsudoku.model.solver.max_matching.call(null,graph,cljs.core.rest.call(null,nexts),cljs.core.conj.call(null,deja_match,cljs.core.first.call(null,match)));
if((cljs.core.count.call(null,sub_res) > (0))){
var G__11173 = cljs.core.rest.call(null,match);
var G__11174 = mrsudoku.model.solver.map_merge.call(null,this_res,cljs.core.PersistentArrayMap.createAsIfByAssoc([cljs.core.first.call(null,nexts),cljs.core.PersistentHashSet.createAsIfByAssoc([cljs.core.first.call(null,match)])]));
var G__11175 = mrsudoku.model.solver.map_merge.call(null,res,sub_res);
match = G__11173;
this_res = G__11174;
res = G__11175;
continue;
} else {
var G__11176 = cljs.core.rest.call(null,match);
var G__11177 = this_res;
var G__11178 = res;
match = G__11176;
this_res = G__11177;
res = G__11178;
continue;
}
} else {
return mrsudoku.model.solver.map_merge.call(null,res,this_res);
}
break;
}
}
});

mrsudoku.model.solver.max_matching.cljs$lang$maxFixedArity = 3;

mrsudoku.model.solver.gen_dom_row = (function mrsudoku$model$solver$gen_dom_row(grid,nb_row){
var dom = mrsudoku.model.solver.gen_lines_par_rows.call(null,grid,nb_row);
var lines = mrsudoku.model.solver.gen_graph_lines.call(null,dom);
var graph = mrsudoku.model.solver.gen_graph.call(null,lines);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [nb_row,mrsudoku.model.solver.max_matching.call(null,graph)], null);
});
mrsudoku.model.solver.affiche_dom = (function mrsudoku$model$solver$affiche_dom(doms){
var tmp = doms;
while(true){
if(cljs.core.seq.call(null,tmp)){
cljs.core.println.call(null,cljs.core.first.call(null,tmp));

var G__11179 = cljs.core.rest.call(null,tmp);
tmp = G__11179;
continue;
} else {
return null;
}
break;
}
});
mrsudoku.model.solver.chercher_min_contraint = (function mrsudoku$model$solver$chercher_min_contraint(doms){
var tmp = doms;
var x = (1);
var y = (1);
var vals = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 10, [(7),null,(1),null,(4),null,(6),null,(3),null,(2),null,(9),null,(5),null,(10),null,(8),null], null), null);
while(true){
if(cljs.core.seq.call(null,tmp)){
var vec__11181 = cljs.core.first.call(null,tmp);
var yy = cljs.core.nth.call(null,vec__11181,(0),null);
var matchs = cljs.core.nth.call(null,vec__11181,(1),null);
if(cljs.core._EQ_.call(null,(0),cljs.core.count.call(null,matchs))){
var G__11187 = cljs.core.rest.call(null,tmp);
var G__11188 = x;
var G__11189 = y;
var G__11190 = vals;
tmp = G__11187;
x = G__11188;
y = G__11189;
vals = G__11190;
continue;
} else {
var vec__11184 = cljs.core.first.call(null,cljs.core.sort_by.call(null,((function (tmp,x,y,vals,vec__11181,yy,matchs){
return (function (p1__11180_SHARP_){
return cljs.core.count.call(null,cljs.core.val.call(null,p1__11180_SHARP_));
});})(tmp,x,y,vals,vec__11181,yy,matchs))
,matchs));
var xx = cljs.core.nth.call(null,vec__11184,(0),null);
var val = cljs.core.nth.call(null,vec__11184,(1),null);
if((cljs.core.count.call(null,val) < cljs.core.count.call(null,vals))){
var G__11191 = cljs.core.rest.call(null,tmp);
var G__11192 = xx;
var G__11193 = yy;
var G__11194 = val;
tmp = G__11191;
x = G__11192;
y = G__11193;
vals = G__11194;
continue;
} else {
var G__11195 = cljs.core.rest.call(null,tmp);
var G__11196 = x;
var G__11197 = y;
var G__11198 = vals;
tmp = G__11195;
x = G__11196;
y = G__11197;
vals = G__11198;
continue;
}
}
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(x - (10)),y,vals], null);
}
break;
}
});
mrsudoku.model.solver.gen_doms = (function mrsudoku$model$solver$gen_doms(grid){
return cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.call(null,(function (p1__11199_SHARP_){
return mrsudoku.model.solver.gen_dom_row.call(null,grid,p1__11199_SHARP_);
}),cljs.core.range.call(null,(1),(10))));
});
mrsudoku.model.solver.update_map = (function mrsudoku$model$solver$update_map(var_args){
var G__11201 = arguments.length;
switch (G__11201) {
case 2:
return mrsudoku.model.solver.update_map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return mrsudoku.model.solver.update_map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

mrsudoku.model.solver.update_map.cljs$core$IFn$_invoke$arity$2 = (function (m,f){
return cljs.core.reduce_kv.call(null,(function (m__$1,k,v){
return cljs.core.assoc.call(null,m__$1,k,f.call(null,v));
}),cljs.core.PersistentArrayMap.EMPTY,m);
});

mrsudoku.model.solver.update_map.cljs$core$IFn$_invoke$arity$3 = (function (m,f,keys){
return cljs.core.reduce_kv.call(null,(function (m__$1,k,v){
if(cljs.core.contains_QMARK_.call(null,keys,k)){
return cljs.core.assoc.call(null,m__$1,k,f.call(null,v));
} else {
return cljs.core.assoc.call(null,m__$1,k,v);
}
}),cljs.core.PersistentArrayMap.EMPTY,m);
});

mrsudoku.model.solver.update_map.cljs$lang$maxFixedArity = 3;

mrsudoku.model.solver.row_enlever = (function mrsudoku$model$solver$row_enlever(doms,y,vals){
return cljs.core.update.call(null,doms,y,(function (p1__11203_SHARP_){
return mrsudoku.model.solver.update_map.call(null,p1__11203_SHARP_,(function (m){
return cljs.core.disj.call(null,m,vals);
}));
}));
});
mrsudoku.model.solver.col_enlever = (function mrsudoku$model$solver$col_enlever(doms,x,vals){
return mrsudoku.model.solver.update_map.call(null,doms,(function (p1__11204_SHARP_){
return mrsudoku.model.solver.update_map.call(null,p1__11204_SHARP_,(function (m){
return cljs.core.disj.call(null,m,vals);
}),cljs.core.PersistentHashSet.createAsIfByAssoc([x]));
}));
});
mrsudoku.model.solver.block_enlever = (function mrsudoku$model$solver$block_enlever(doms,cx,cy,vals){
var block_x = ((10) + (((3) * cljs.core.quot.call(null,(cx - (1)),(3))) + (1)));
var block_y = (((3) * cljs.core.quot.call(null,(cy - (1)),(3))) + (1));
var rx = cljs.core.into.call(null,cljs.core.PersistentHashSet.EMPTY,cljs.core.range.call(null,block_x,((3) + block_x)));
var ry = cljs.core.into.call(null,cljs.core.PersistentHashSet.EMPTY,cljs.core.range.call(null,block_y,((3) + block_y)));
return mrsudoku.model.solver.update_map.call(null,doms,((function (block_x,block_y,rx,ry){
return (function (p1__11205_SHARP_){
return mrsudoku.model.solver.update_map.call(null,p1__11205_SHARP_,((function (block_x,block_y,rx,ry){
return (function (m){
return cljs.core.disj.call(null,m,vals);
});})(block_x,block_y,rx,ry))
,rx);
});})(block_x,block_y,rx,ry))
,ry);
});
mrsudoku.model.solver.doms_remplir = (function mrsudoku$model$solver$doms_remplir(doms,x,y,vals){
var a = cljs.core.update.call(null,doms,y,(function (p1__11206_SHARP_){
return cljs.core.dissoc.call(null,p1__11206_SHARP_,((10) + x));
}));
var b = mrsudoku.model.solver.row_enlever.call(null,a,y,vals);
var c = mrsudoku.model.solver.col_enlever.call(null,b,(x + (10)),vals);
var d = mrsudoku.model.solver.block_enlever.call(null,c,x,y,vals);
return d;
});
/**
 * Solve the sudoku `grid` by returing a full solved grid,
 *  or `nil` if the solver fails.
 */
mrsudoku.model.solver.solve = (function mrsudoku$model$solver$solve(var_args){
var G__11208 = arguments.length;
switch (G__11208) {
case 1:
return mrsudoku.model.solver.solve.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return mrsudoku.model.solver.solve.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

mrsudoku.model.solver.solve.cljs$core$IFn$_invoke$arity$1 = (function (grid){
var doms = mrsudoku.model.solver.gen_doms.call(null,grid);
return mrsudoku.model.solver.solve.call(null,grid,doms);
});

mrsudoku.model.solver.solve.cljs$core$IFn$_invoke$arity$2 = (function (grid,doms){
var vec__11209 = mrsudoku.model.solver.chercher_min_contraint.call(null,doms);
var x = cljs.core.nth.call(null,vec__11209,(0),null);
var y = cljs.core.nth.call(null,vec__11209,(1),null);
var vals = cljs.core.nth.call(null,vec__11209,(2),null);
var solveable = (cljs.core.first.call(null,vals) > (0));
var solver = cljs.core._EQ_.call(null,cljs.core.count.call(null,vals),(10));
if(solver){
return grid;
} else {
if(solveable){
var tmp_vals = vals;
while(true){
if(cljs.core.seq.call(null,tmp_vals)){
var tmp_grid = mrsudoku.model.grid.change_cell.call(null,grid,x,y,mrsudoku.model.grid.mk_cell.call(null,new cljs.core.Keyword(null,"set","set",304602554),cljs.core.first.call(null,tmp_vals)));
var solve_grid = mrsudoku.model.solver.solve.call(null,tmp_grid,mrsudoku.model.solver.doms_remplir.call(null,doms,x,y,cljs.core.first.call(null,tmp_vals)));
if(cljs.core.truth_(solve_grid)){
return solve_grid;
} else {
var G__11213 = cljs.core.rest.call(null,tmp_vals);
tmp_vals = G__11213;
continue;
}
} else {
return null;
}
break;
}
} else {
return null;
}
}
});

mrsudoku.model.solver.solve.cljs$lang$maxFixedArity = 2;


//# sourceMappingURL=solver.js.map
